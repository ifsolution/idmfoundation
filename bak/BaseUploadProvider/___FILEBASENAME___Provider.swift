//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//  ___COPYRIGHT___
//

import Foundation
import IDMFoundation

typealias ___VARIABLE_moduleName___BaseProvider = RootAnyProvider<UploadURLsParameter>
typealias ___VARIABLE_moduleName___Provider = BaseUploadProvider<UploadURLsParameter>

class ___VARIABLE_moduleName___UploadProvider: ___VARIABLE_moduleName___Provider {
    override func requestPath(parameters: UploadURLsParameter?) -> String {
        return <#code#>
    }
}
